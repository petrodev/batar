# Batar

An old school space shooter

## Online resources used

- Voices: https://speechgen.io/
- Ships inspiration & basic meshes: https://ship.shapewright.com/
- Materials & textures: https://ambientcg.com/list?type=Material
- Illustrations: https://perchance.org/
- Some sound FX: https://pixabay.com/sound-effects/search/

## Software used

- Of course: Godot 4.2.1
- Text editor: [Emacs on Eglot/LSP](https://hristos.lol/blog/godot-engine-editing-gdscript-with-emacs/)
- 3D modeling & texturing: [Blender](https://www.blender.org/) 4.1
- Vector graphics: [Inkscape](https://inkscape.org/) 1.1
- Bitmap graphics: [GIMP](https://www.gimp.org/) 2.10.30
- Music: [Sunvox](https://warmplace.ru/soft/sunvox/) 2.1.1c

## TODO

- Still one shuttle short
- Mother sounds
- NewWeapon sound consolidation
- Mother multiple muzzles
- Unfinished walls
- Ship's crosshair only on mothership anim finished
